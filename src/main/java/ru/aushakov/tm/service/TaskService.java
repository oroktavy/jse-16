package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.ITaskService;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.empty.EmptyDescriptionException;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.empty.EmptyNameException;
import ru.aushakov.tm.exception.entity.NoEntityProvidedException;
import ru.aushakov.tm.exception.general.InvalidIndexException;
import ru.aushakov.tm.exception.general.InvalidStatusException;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new NoEntityProvidedException("task", "add");
        taskRepository.add(task);
    }

    @Override
    public Task add(final String name, final String description) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        if (StringUtils.isEmpty(description)) throw new EmptyDescriptionException();
        final Task task = new Task(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new NoEntityProvidedException("task", "remove");
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task updateOneById(final String id, final String name, final String description) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.updateOneById(id, name, description);
    }

    @Override
    public Task updateOneByIndex(final Integer index, final String name, final String description) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.updateOneByIndex(index, name, description);
    }

    @Override
    public Task startOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.startOneById(id);
    }

    @Override
    public Task startOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return taskRepository.startOneByIndex(index);
    }

    @Override
    public Task startOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.startOneByName(name);
    }

    @Override
    public Task finishOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.finishOneById(id);
    }

    @Override
    public Task finishOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return taskRepository.finishOneByIndex(index);
    }

    @Override
    public Task finishOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.finishOneByName(name);
    }

    @Override
    public Task changeOneStatusById(final String id, final String statusId) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return taskRepository.changeOneStatusById(id, status);
    }

    @Override
    public Task changeOneStatusByIndex(final Integer index, final String statusId) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return taskRepository.changeOneStatusByIndex(index, status);
    }

    @Override
    public Task changeOneStatusByName(final String name, final String statusId) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return taskRepository.changeOneStatusByName(name, status);
    }

    @Override
    public Task assignTaskToProject(final String taskId, final String projectId) {
        if (StringUtils.isEmpty(taskId)) throw new EmptyIdException("task");
        if (StringUtils.isEmpty(projectId)) throw new EmptyIdException("project");
        return taskRepository.assignTaskToProject(taskId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        if (StringUtils.isEmpty(taskId)) throw new EmptyIdException("task");
        return taskRepository.unbindTaskFromProject(taskId);
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) {
        if (StringUtils.isEmpty(projectId)) throw new EmptyIdException("project");
        return taskRepository.findAllTasksByProjectId(projectId);
    }

}

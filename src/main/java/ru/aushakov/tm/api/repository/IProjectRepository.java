package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateOneById(String id, String name, String description);

    Project updateOneByIndex(Integer index, String name, String description);

    Project startOneById(String id);

    Project startOneByIndex(Integer index);

    Project startOneByName(String name);

    Project finishOneById(String id);

    Project finishOneByIndex(Integer index);

    Project finishOneByName(String name);

    Project changeOneStatusById(String id, Status status);

    Project changeOneStatusByIndex(Integer index, Status status);

    Project changeOneStatusByName(String name, Status status);

}

package ru.aushakov.tm.controller;

import ru.aushakov.tm.api.controller.IProjectController;
import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.enumerated.SortType;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.model.Project;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        List<Project> projects;
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + " OR PRESS ENTER:");
        final SortType sortType = SortType.toSortType(TerminalUtil.nextLine());
        if (sortType == null) projects = projectService.findAll();
            else projects = projectService.findAll(sortType.getComparator());
        if (projects == null || projects.size() < 1) {
            System.out.println("[NOTHING FOUND]");
            return;
        }
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.add(name, description);
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("CREATED: " + project.getCreated());
        System.out.println("START DATE: " + project.getStartDate());
        System.out.println("END DATE: " + project.getEndDate());
    }

    @Override
    public void showOneById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showOneByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showOneByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void removeOneById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeOneByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void updateOneById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateOneById(id, name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void updateOneByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateOneByIndex(index, name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startOneById() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startOneById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startOneByIndex() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startOneByName() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishOneById() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishOneById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishOneByIndex() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishOneByName() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeOneStatusById() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS " + Arrays.toString(Status.values()) + ":");
        final String statusId = TerminalUtil.nextLine();
        final Project project = projectService.changeOneStatusById(id, statusId);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeOneStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS " + Arrays.toString(Status.values()) + ":");
        final String statusId = TerminalUtil.nextLine();
        final Project project = projectService.changeOneStatusByIndex(index, statusId);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeOneStatusByName() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS " + Arrays.toString(Status.values()) + ":");
        final String statusId = TerminalUtil.nextLine();
        final Project project = projectService.changeOneStatusByName(name, statusId);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void deepDeleteProjectById() {
        System.out.println("[REMOVE PROJECT WITH CHILD TASKS]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.deepDeleteProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

}

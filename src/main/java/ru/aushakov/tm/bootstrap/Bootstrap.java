package ru.aushakov.tm.bootstrap;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.controller.ICommandController;
import ru.aushakov.tm.api.controller.IProjectController;
import ru.aushakov.tm.api.controller.ITaskController;
import ru.aushakov.tm.api.repository.ICommandRepository;
import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.ICommandService;
import ru.aushakov.tm.api.service.ILoggerService;
import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.api.service.ITaskService;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.controller.CommandController;
import ru.aushakov.tm.controller.ProjectController;
import ru.aushakov.tm.controller.TaskController;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.general.UnknownArgumentException;
import ru.aushakov.tm.exception.general.UnknownCommandException;
import ru.aushakov.tm.repository.CommandRepository;
import ru.aushakov.tm.repository.ProjectRepository;
import ru.aushakov.tm.repository.TaskRepository;
import ru.aushakov.tm.service.CommandService;
import ru.aushakov.tm.service.LoggerService;
import ru.aushakov.tm.service.ProjectService;
import ru.aushakov.tm.service.TaskService;
import ru.aushakov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ILoggerService loggerService = new LoggerService();

    private void initData() {
        taskService.add("DEMO 2", "222222222222").setStatus(Status.IN_PROGRESS);
        taskService.add("DEMO 3", "333333333333");
        taskService.add("DEMO 1", "111111111111").setStatus(Status.COMPLETED);
        projectService.add("DEMO 2", "222222222222").setStatus(Status.IN_PROGRESS);
        projectService.add("DEMO 3", "333333333333");
        projectService.add("DEMO 1", "111111111111").setStatus(Status.COMPLETED);
    }

    public void run(final String[] args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        initData();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            }
            catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArg(final String arg) {
        if (StringUtils.isEmpty(arg)) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAppInfo();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                throw new UnknownArgumentException(arg);
        }
    }

    public void parseCommand(final String command) {
        if (StringUtils.isEmpty(command)) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                commandController.showAppInfo();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showOneById();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showOneByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showOneByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeOneById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeOneByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeOneByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateOneById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateOneByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startOneById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startOneByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startOneByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.finishOneById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.finishOneByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.finishOneByName();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeOneStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeOneStatusByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME:
                taskController.changeOneStatusByName();
                break;
            case TerminalConst.TASK_ASSIGN_TO_PROJECT:
                taskController.assignTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                taskController.unbindTaskFromProject();
                break;
            case TerminalConst.TASK_FIND_ALL_BY_PROJECT_ID:
                taskController.findAllTasksByProjectId();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showOneById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showOneByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showOneByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeOneById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeOneByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeOneByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateOneById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateOneByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startOneById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startOneByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startOneByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.finishOneById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.finishOneByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.finishOneByName();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeOneStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeOneStatusByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME:
                projectController.changeOneStatusByName();
                break;
            case TerminalConst.PROJECT_DEEP_DELETE_BY_ID:
                projectController.deepDeleteProjectById();
                break;
            default:
                throw new UnknownCommandException(command);
        }
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        if (args.length > 1) System.out.println("NOTE: Only one argument is supported at a time!");
        return true;
    }

}

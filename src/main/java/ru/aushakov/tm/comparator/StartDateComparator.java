package ru.aushakov.tm.comparator;

import ru.aushakov.tm.api.entity.IHasStartDate;

import java.util.Comparator;
import java.util.Date;

public final class StartDateComparator implements Comparator<IHasStartDate> {

    private static final StartDateComparator INSTANCE = new StartDateComparator();

    private StartDateComparator() {
    }

    public static StartDateComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasStartDate o1, final IHasStartDate o2) {
        if (o1 == null || o2 == null) return ((o1 == null) ? ((o2 == null) ? 0 : -1) : 1);
        final Date startDate1 = o1.getStartDate();
        final Date startDate2 = o2.getStartDate();
        if (startDate1 == null || startDate2 == null)
            return ((startDate1 == null) ? ((startDate2 == null) ? 0 : -1) : 1);
        return startDate1.compareTo(startDate2);
    }

}

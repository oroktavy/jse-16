package ru.aushakov.tm.exception.entity;

public class NoEntityProvidedException extends RuntimeException{

    public NoEntityProvidedException(final String entityName, final String operation) {
        super("No " + entityName + " provided to " + operation + "!");
    }

}

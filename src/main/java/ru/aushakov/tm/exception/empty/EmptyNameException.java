package ru.aushakov.tm.exception.empty;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Provided name is empty!");
    }

}
